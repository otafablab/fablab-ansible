#!/usr/bin/env python
import os, json

def tf_output_to_ansible(tf_output):
    value = tf_output['ip']['value']
    data = {
        '_meta': {
            'hostvars': {}
        },
        'all': {
            'children': [],
            'vars': {
                'ansible_ssh_user': 'localadmin'
            }
        },
    }
    for groupname, hosts in value.items():
        data[groupname] = {
            'hosts': list(hosts.keys())
        }
        for hostname, ip in hosts.items():
            data['_meta']['hostvars'][hostname] = {
                'ansible_host': ip
            }
        data['all']['children'].append(groupname)
    return data

if __name__ == '__main__':
    path = os.path.join(os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__))), "tf-output.json")
    with open(path) as f:
        ansible = tf_output_to_ansible(json.loads(f.read()))
        print(json.dumps(ansible))
